const express = require('express');
const router = express.Router();
const db = require('../database/connection');

router.post('/', async (req, res) => {
    await db.task('uploadLog', async t => {
        const logID = await t.one(
            'INSERT INTO logs.log (commander, type, body, "startedAt", comment) VALUES (${commander}, ${type}, ${body}, ${startedAt}, ${comment}) RETURNING id',
            {
                commander: req.body.commander,
                type: req.body.type,
                body: req.body.body,
                startedAt: req.body.startedAt,
                comment: req.body.comment,
            },
            (u) => u.id,
        );

        for (const journal of req.body.journal) {
            await t.none(
                'INSERT INTO logs.log_journal ("logID", journal) VALUES (${id}, ${journal})',
                {
                    id: logID,
                    journal: journal,
                },
            );
        }
    });

    res.send(`Uploaded ${req.body.journal.length} entries`);
});

router.get('/', async (req, res) => {
    let rows = await db.manyOrNone(`
    SELECT
        id,
        commander,
        "type",
        "startedAt",
        "comment",
        body
    FROM
        logs.log;
    `);
    res.json(rows);
});

router.get('/:logID', async (req, res) => {
    const logs = await db.many(
        'SELECT journal FROM logs.log_journal WHERE "logID" = ${logID} ORDER BY id ASC',
        { logID: req.params.logID },
    )

    let jsonl = '';
    for (const log of logs) {
        jsonl += log.journal + "\n";
    }

    res.set({
        'Content-Disposition': `attachment; filename=log_export_${req.params.logID}.log`,
        'Content-Type': 'text/plain',
    });
    res.send(jsonl);
});


module.exports = router;
