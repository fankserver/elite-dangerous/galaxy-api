const express = require('express');
const router = express.Router();
const db = require('../database/connection');

router.get('/', async (req, res) => {
    let rows = await db.manyOrNone(`
    SELECT
        overlap.id,
        overlap."systemName",
        overlap."systemID64",
        overlap."bodyName",
        overlap.commodity,
        overlap."overlaps" as "overlapCount",
        overlap.reporter,
        overlap.description,
        overlap."createdAt",
        overlap."updatedAt",
        CAST((CASE
            WHEN overlap."approverID" != '' THEN 1
            ELSE 0
        END) AS BOOLEAN) as approved
    FROM
        elitetracker."overlaps" overlap;
    `);
    res.json(rows);
});
router.get('/near/:systemName', async (req, res) => {
    let rows = await db.manyOrNone(`
    SET search_path TO elite, "$user", public;
    SELECT
        systems.name AS "systemName",
        systems.id64 AS "systemID64",
        systems.coords::cube<->(SELECT coords::cube FROM elite.systems WHERE lower(name) = lower($<systemName>) limit 1) AS "systemDistance",
        overlap."bodyName" AS "bodyName",
        bodies.distance AS "bodyDistanceToStar",
        REPLACE(COALESCE(bodies.reserve, ''), 'Resources', '') AS "bodyReserve",
        bodies.ring_type AS "bodyRingTypes",
        overlap.id as "overlapID",
        overlap.commodity AS "commodity",
        overlap."overlaps" AS "overlapCount",
        overlap.description AS description,
        overlap."createdAt" AS "createdAt",
        overlap.reporter AS reporter,
        CAST((CASE
            WHEN overlap."approverID" != '' THEN 1
            ELSE 0
        END) AS BOOLEAN) as approved
    FROM
        elitetracker.overlaps overlap
        INNER JOIN elite.systems systems on systems.id64 = overlap."systemID64"
        INNER JOIN elite.planets bodies ON bodies.sys_id64 = systems.id64 AND bodies."name" = overlap."bodyName"
    ORDER BY 2,4
    LIMIT 200;
    `, {
        systemName: req.params.systemName,
    });
    res.json(rows);
});
router.get('/export/tripn', async (req, res) => {
    let rows = await db.manyOrNone(`
    SET search_path TO elite, "$user", public;
    SELECT
        systems.name AS "systemName",
        systems.id64 AS "systemID64",
        cube_ll_coord(systems.coords, 1) AS "systemCoordX",
        cube_ll_coord(systems.coords, 2) AS "systemCoordY",
        cube_ll_coord(systems.coords, 3) AS "systemCoordZ",
        systems.coords::cube<->(SELECT coords::cube FROM elite.systems WHERE lower(name) = lower('Sol') limit 1) AS "distanceFromSol",
        overlap."bodyName" AS "bodyName",
        bodies.distance AS "distanceToStar",
        overlap.id as "overlapID",
        overlap.commodity AS "commodity",
        overlap."overlaps" AS "overlapCount",
        overlap.description AS description,
        overlap."createdAt" AS "createdAt",
        overlap."updatedAt" AS "updatedAt",
        overlap.reporter AS reporter,
        CAST((CASE
            WHEN overlap."approverID" != '' THEN 1
            ELSE 0
        END) AS BOOLEAN) as approved
    FROM
        elitetracker.overlaps overlap
        INNER JOIN elite.systems systems on systems.id64 = overlap."systemID64"
        INNER JOIN elite.planets bodies ON bodies.sys_id64 = systems.id64 AND bodies."name" = overlap."bodyName"
    WHERE
        overlap.commodity = 'Tritium'
        AND overlap.reporter != 'TriPN'
    `);
    res.json(rows);
})

module.exports = router;
