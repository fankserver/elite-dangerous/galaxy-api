const express = require('express');
const {google} = require('googleapis');
const googleapi = require('../googleapi');
const db = require('../database/connection');

function parseBody(body) {
    if (body.match(/A Ring/i)) {
        return { name: body.replace(/A Ring/i, '').trim(), ring: 'A' };
    }
    else if (body.match(/B Ring/i)) {
        return { name: body.replace(/B Ring/i, '').trim(), ring: 'B' };
    }
    else if (body.match(/C Ring/i)) {
        return { name: body.replace(/C Ring/i, '').trim(), ring: 'C' };
    }
    else if (body.match(/Ring/i)) {
        return { name: body.replace(/Ring/i, '').trim(), ring: '' };
    }
    return { name: body.trim() };
}

const router = express.Router();

router.get('/tripn', async (req, res) => {
    try {
        const auth = googleapi.authorize();
        const sheets = google.sheets({ version: 'v4', auth });
        const spreadsheet = await sheets.spreadsheets.values.get({
            spreadsheetId: '1DfcVCHYgPHZnxUmsrGWXKdNK-2iz7t2MG80EGGW9XGI',
            range: 'DB!A2:K',
        });

        const rows = spreadsheet.data.values;
        if (rows.length) {
            for (const rowID in rows) {
                const row = rows[rowID];

                const known = await db.one(`
                    SELECT
                        COUNT(*) as found
                    FROM
                        elitetracker.overlaps
                    WHERE
                        "reporterID" = 'TriPN_row_' || $<rowID>;
                `, { rowID });
                if (known.found > 0) {
                    console.log('found row ' + rowID);
                    continue;
                }

                const commodity = 'Tritium';
                let overlapCount = 0;
                if (row[7].includes('Tri2')) {
                    overlapCount = 2;
                } else if (row[7].includes('Tri3')) {
                    overlapCount = 3;
                } else if (row[7].includes('Tri4')) {
                    overlapCount = 4;
                }
                const body = parseBody(row[5]);
                console.log(`${commodity}x${overlapCount} at ${body.name} (${body.ring})`);

                const foundOverlaps = await db.manyOrNone(`
                    SELECT
                        id
                    FROM
                        elitetracker.overlaps
                    WHERE
                        "bodyName" = $<bodyName>
                        AND commodity = $<commodity>
                        AND "overlaps" = $<overlapCount>
                `, {
                    commodity,
                    bodyName: body.name,
                    overlapCount,
                });
                if (foundOverlaps.length > 0) {
                    console.log('found overlaps ' + rowID);
                    continue;
                }

                let description = row[7] + "\n\n" + row[10];

                await db.none(`
                    SET random_page_cost = 1;
                    INSERT INTO elitetracker.overlaps (
                        "systemName",
                        "systemID64",
                        "bodyName",
                        commodity,
                        "overlaps",
                        reporter,
                        "reporterID",
                        "approverID",
                        description,
                        "createdAt",
                        "updatedAt"
                    )
                    SELECT
                        systems."name",
                        systems.id64,
                        planets."name",
                        $<commodity>,
                        $<overlapCount>,
                        'TriPN',
                        'TriPN_row_' || $<rowID>,
                        'TriPN_row_' || $<rowID>,
                        $<description>,
                        NOW(),
                        NOW()
                    FROM
                        elite.planets planets
                        INNER JOIN elite.systems systems ON systems.id64 = planets.sys_id64
                    WHERE
                        lower(planets.name) = LOWER($<bodyName>)
                `, {
                    rowID,
                    commodity,
                    bodyName: body.name,
                    overlapCount,
                    description,
                });
            }
        } else {
            console.log('No data found.');
        }

        res.send(``);
    }
    catch(err) {
        res.send(`Error: `+ err);
    }
});

module.exports = router;
