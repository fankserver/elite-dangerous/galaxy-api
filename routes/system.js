const express = require('express');
const router = express.Router();
const db = require('../database/connection');

router.get('/autocomplete/:systemName', async (req, res) => {
    let rows = await db.manyOrNone(`
    SELECT
        name AS "systemName",
        id64 AS "systemID64"
    FROM
        elite.systems
    WHERE
        lower(name) LIKE $<systemName>
    ORDER BY
        name ASC
    LIMIT 10
    `, {
        systemName: req.params.systemName.toLowerCase() + '%',
    });
    res.json(rows);
});

module.exports = router;
