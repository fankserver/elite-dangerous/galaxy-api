const { db } = require((process.env.CONFIG_PATH || '..') + '/config.json');

const pgp = require('pg-promise')(/*{
    query(e) {
        console.log(e.query);
    }
}*/);
const connection = pgp(db);

module.exports = connection;
