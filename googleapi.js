const fs = require('fs');
const {google} = require('googleapis');

const credentialFile = (process.env.CONFIG_PATH || '.') + '/google-credentials.json';
const credentials = JSON.parse(fs.readFileSync(credentialFile).toString());

const tokenFile = (process.env.CONFIG_PATH || '.') + '/google-token.json';
const token = JSON.parse(fs.readFileSync(tokenFile).toString());

module.exports = {
    authorize() {
        const {client_secret, client_id, redirect_uris} = credentials.installed;
        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);

        oAuth2Client.setCredentials(token);
        return oAuth2Client;
    }
}
